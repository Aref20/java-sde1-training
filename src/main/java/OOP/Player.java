package OOP;

public class Player {
	
	 String name;
	 int score;
	 Card[] cards;
	
	public Player(String name, int score, Card[] cards) {
		super();
		this.name = name;
		this.score = score;
		this.cards = cards;
	}

}
