
import java.util.Arrays;
import java.util.Scanner; 
import java.io.FileWriter;   // Import the FileWriter class
import java.io.IOException;  // Import the IOException class to handle errors
public class Hello {
	  public static void main(String args[]){
		    long sum= 0;
		    float avg = 0;
		    int  number ;  
		    int m1=0,  m2=0,  m3=0,n1=0,n2=0,n3=0;
		    Scanner sc = new Scanner(System.in); 

		    //validate user input
		    do {
		        System.out.println("Please enter a positive number!");
		        while (!sc.hasNextInt()) {
		            System.out.println("That's not a number!");
		            sc.next();
		        }
		        number = sc.nextInt();
		    } while (number <= 0);
		    
		    //initialize an array
		    int [] arr ,fr ;
		    arr = fr =  new int[number];

		 
		    for(int i=0;i<number;i++) {
		    	arr[i] = (int)(Math.random() * 1001);
		    	sum+=arr[i];
		    	
		    }
		    
		    

		    
		    
		    boolean visited[] = new boolean[number];
		     
		    Arrays.fill(visited, false);
		 

		    for (int i = 0; i < number; i++) {
		 
		        if (visited[i] == true)
		            continue;
		 
		        int count = 1;
		        for (int j = i + 1; j < number; j++) {
		            if (arr[i] == arr[j]) {
		                visited[j] = true;
		                count++;
		            }
		        }

		        if (count >m1 ) {
		        	m3 = m2;
		        	m2 = m1;
		        	m1 = count;
		        	n1  = arr[i];
		        }
		        else if (count > m2) {
		                m3 = m2;
		                m2 = count;
		                n2  = arr[i];
		            }
		        
		        else if (count > m3)
	                m3 = count;
		        	n3  = arr[i];
		        
		    }
		    
		    
		    avg =  ((float)sum / (float) arr.length);
		    try {
		        FileWriter myWriter = new FileWriter("results.txt");
		        myWriter.write("sum = " + sum);
		        myWriter.write(System.getProperty( "line.separator" ));
		        myWriter.write("average = " + avg);
		        myWriter.write(System.getProperty( "line.separator" ));
		        myWriter.write("num1 = "+ n1 +" freq = " + m1);
		        myWriter.write(System.getProperty( "line.separator" ));
		        myWriter.write("num2 = "+ n2+ " freq = " + m2);
		        myWriter.write(System.getProperty( "line.separator" ));
		        myWriter.write("num3 = "+ n3+ " freq = " + m3);
		        
		        myWriter.close();
		        System.out.println("Successfully wrote to the file.");
		      } catch (IOException e) {
		        System.out.println("An error occurred.");
		        e.printStackTrace();
		      }

		    
		    
		  }
}
